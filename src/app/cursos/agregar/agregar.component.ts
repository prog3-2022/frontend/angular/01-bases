import {Component, OnInit} from '@angular/core';
import {Curso} from '../interfaces/curso.interface';
import {CursosService} from '../services/cursos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  nuevo: Curso = {
    nombre: '',
    descripcion: ''
  };

  constructor(private cursosService: CursosService) {
  }

  ngOnInit(): void {
  }

  agregar() {
    this.cursosService.agregarCurso(this.nuevo)
    this.nuevo = {
      nombre: '',
      descripcion: ''
    };
  }
}
