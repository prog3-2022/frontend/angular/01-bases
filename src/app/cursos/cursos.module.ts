import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrincipalComponent} from './principal/principal.component';
import {FormsModule} from "@angular/forms";
import { ListadoComponent } from './listado/listado.component';
import { AgregarComponent } from './agregar/agregar.component';

@NgModule({
  declarations: [
    PrincipalComponent,
    ListadoComponent,
    AgregarComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    PrincipalComponent
  ]
})
export class CursosModule {
}
