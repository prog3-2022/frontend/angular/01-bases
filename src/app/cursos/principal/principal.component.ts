import {Component, OnInit} from '@angular/core';
import {Curso} from "../interfaces/curso.interface";

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
