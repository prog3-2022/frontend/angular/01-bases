import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saludo',
  templateUrl: './saludo.component.html',
  styleUrls: ['./saludo.component.css']
})
export class SaludoComponent implements OnInit {
  nombre: string = 'Manu';
  apellido: string = 'Ginobili';
  bienvenida = '';
  despedida = '';

  getBienvenida() {
    this.bienvenida = `Hola ${this.nombre} ${this.apellido}`.toUpperCase();
  }

  getDespedida() {
    this.despedida = `Chau ${this.nombre} ${this.apellido}`.toUpperCase();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
