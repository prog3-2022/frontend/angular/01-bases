import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {PersonasModule} from './personas/personas.module';
import {SaludoModule} from './saludo/saludo.module';
import {CursosModule} from "./cursos/cursos.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PersonasModule,
    SaludoModule,
    CursosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
