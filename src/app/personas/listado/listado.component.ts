import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  personas: string[] = ['Pablo Huguet', 'Carlos Sanchez', 'Pedro Torres'];
  existePersonas = true;

  constructor() {
  }

  ngOnInit(): void {
  }

  eliminar() {
    this.personas.shift();
    if (this.personas.length <= 0) {
      this.existePersonas = false;
    }
  }
}
