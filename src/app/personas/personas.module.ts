import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListadoComponent} from './listado/listado.component';
import {PersonaComponent} from './persona/persona.component';


@NgModule({
  declarations: [
    ListadoComponent,
    PersonaComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ListadoComponent,
    PersonaComponent
  ]
})
export class PersonasModule {
}
